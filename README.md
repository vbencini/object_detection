# Object detection

In this project we show an example of how to use an object detection model form HuggingFace

- Image processor = facebook/detr-resnet-50
- Model = facebook/detr-resnet-50

## Useful infro before we start

In order to run the project you need an HuggingFace token. 

The HuggingFace token can be generated for free. It will be sufficient to create an account. See instructions https://huggingface.co/docs/hub/security-tokens

## Installation

The application does not have a builder. You can simply clone the repository:

```bash
git clone https://gitlab.cern.ch/vbencini/object_detection.git 
```

Create and activate a virtual environment in Python:

```bash
 python -m venv /my_env
 source /my_env/bin/activate
```

or Anaconda

```bash
 conda create -n my_env python=3.9
 conda activate my_env
```

Then you can install the required packages executing the command:

```bash
 pip install -r requirements.txt
```
## Usage

To use the app simply run the following command:
```bash
python app.py
```

In alternative you can run it on the Jupyter notebook named test_od.ipynb
